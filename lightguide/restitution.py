import numpy as num
from scipy import signal
from .utils import traces_to_numpy_and_meta


def rad_to_de(traces, copy=False, m_per_rad=11.6e-9):
    out_traces = []
    for tr in traces:
        assert "gauge_length" in tr.meta, "gauge_length not in metadata"
        if copy:
            tr = tr.copy()
        tr.ydata *= m_per_rad
        tr.ydata /= tr.meta["gauge_length"] * tr.deltat
        tr.ydata /= 8192
        tr.meta["unit"] = "strain rate (m/s/m)"
        out_traces.append(tr)


def de_to_e(traces, detrend=True, copy=False):
    out_traces = []
    data, _ = traces_to_numpy_and_meta(traces)

    # if detrend:
    #     signal.detrend(data, type='linear', axis=1, overwrite_data=True)
    data = num.cumsum(data, axis=1)

    for itr, tr in enumerate(traces):
        if copy:
            tr = tr.copy()
        tr.set_ydata(data[itr])
        tr.meta["unit"] = "strain (m/m)"
        out_traces.append(tr)

    return out_traces


def de_to_acceleration_static_slowness(traces, slowness, copy=False):
    out_traces = []
    for tr in traces:
        if copy:
            tr = tr.copy()
        tr.set_ydata(tr.ydata / slowness)
        tr.meta["unit"] = "acceleration (m/s^2)"
        out_traces.append(tr)

    return out_traces


def de_to_velocity_static_slowness(traces, slowness, copy=False):
    out_traces = de_to_e(traces, copy)
    for tr in out_traces:
        tr.set_ydata(tr.ydata / slowness)
        tr.meta["unit"] = "velocity (m/s)"

    return out_traces


def de_to_relative_displacement(traces, copy=False):
    traces = de_to_e(traces, copy)
    data, meta = traces_to_numpy_and_meta(traces)

    data = num.cumsum(data, axis=0) * meta.spatial_resolution
    for itr, tr in enumerate(traces):
        tr.set_ydata(data[itr])
        tr.meta["unit"] = "displacement (m)"

    return traces


def de_to_relative_velocity(traces, copy=False):
    traces = de_to_e(traces, copy)
    data, meta = traces_to_numpy_and_meta(traces)

    data = num.diff(data, n=1, axis=0) / meta.spatial_resolution
    for itr, tr in enumerate(traces):
        tr.set_ydata(data[itr])
        tr.meta["unit"] = "velocity (m/s)"

    return traces
