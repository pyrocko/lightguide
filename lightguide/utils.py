import time
from functools import wraps

import numpy as num


class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


def traces_to_numpy_and_meta(traces):
    ntraces = len(traces)
    nsamples = set(tr.ydata.size for tr in traces)

    assert len(nsamples) == 1, "Traces nsamples differ"
    nsamples = nsamples.pop()

    data = num.zeros((ntraces, nsamples))
    for itr, tr in enumerate(traces):
        data[itr, :] = tr.ydata
        meta = tr.meta

    return data, AttrDict(meta)


def timeit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        t = time.time()
        ret = func(*args, **kwargs)
        print(func.__qualname__, time.time() - t)
        return ret

    return wrapper
